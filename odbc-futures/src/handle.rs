use crate::error::{SqlDiagnosticRecord, SqlError, SqlResult};
use crate::util;
use crate::util::VecCapacityExt;
use odbc_sys::*;

pub trait SqlHandle: Drop + Send + Sync + Sized + 'static {
    const TYPE: HandleType;
    type Type: Copy + Sized + 'static;

    unsafe fn typed_handle(&self) -> Self::Type;
    unsafe fn handle(&self) -> SQLHANDLE;

    fn diagnostics_count(&self) -> SqlResult<usize> {
        let mut count: SQLINTEGER = 0;

        let ret = unsafe {
            SQLGetDiagFieldW(
                Self::TYPE,
                self.handle(),
                0, // get a header diagnostic,
                SqlHeaderDiagnosticIdentifier::SQL_DIAG_NUMBER as SQLSMALLINT,
                (&mut count as *mut SQLINTEGER) as SQLPOINTER,
                0, // buffer length is ignored when reading an integer
                std::ptr::null_mut(),
            )
        };

        match ret {
            SQL_SUCCESS => {
                assert!(count >= 0);
                Ok(count as usize)
            }
            SQL_NO_DATA => {
                assert_eq!(0, count);
                Ok(0)
            }
            SQL_ERROR => Err(ret.into()),
            _ => panic!("Unexpected SQLGetDiagFieldW return code: {:?}", ret),
        }
    }

    fn has_diagnostics(&self) -> SqlResult<bool> {
        let count = self.diagnostics_count()?;
        Ok(count > 0)
    }

    fn diagnostics(&self) -> SqlResult<Vec<SqlDiagnosticRecord>> {
        let count = self.diagnostics_count()?;
        let mut results: Vec<SqlDiagnosticRecord> = Vec::with_capacity(count);

        let mut state: [SQLWCHAR; SQL_SQLSTATE_SIZE + 1] = [0 as SQLWCHAR; SQL_SQLSTATE_SIZE + 1];
        let mut native_error: SQLINTEGER = 0;
        let mut message_buffer: Vec<SQLWCHAR> = Vec::with_capacity(SQL_MAX_MESSAGE_LENGTH as usize);
        let mut text_len: SQLSMALLINT = 0;

        for i in 1..=count {
            loop {
                let ret = unsafe {
                    SQLGetDiagRecW(
                        Self::TYPE,
                        self.handle(),
                        i as SQLSMALLINT,
                        state.as_mut_ptr(),
                        &mut native_error,
                        message_buffer.as_mut_ptr(),
                        message_buffer.capacity() as SQLSMALLINT,
                        &mut text_len,
                    )
                };

                match ret {
                    SQL_ERROR => return Err(ret.into()),
                    SQL_SUCCESS => {
                        assert!(text_len >= 0);
                        let text_len = text_len as usize;

                        unsafe {
                            message_buffer.set_len_checked(text_len);
                        }

                        let state_string = util::from_utf_16_null_terminated(&state)?;
                        let message_text = util::from_utf_16_null_terminated(&message_buffer)?;

                        let record = SqlDiagnosticRecord {
                            state: state_string,
                            message: message_text,
                            native_error,
                        };
                        results.push(record);
                        break;
                    }
                    SQL_SUCCESS_WITH_INFO => {
                        assert!(text_len >= 0);
                        message_buffer.reserve_capacity(text_len as usize + 1);
                    }
                    SQL_NO_DATA => return Ok(results),
                    _ => panic!("Unexpected SQLGetDiagRecW return code: {:?}", ret),
                };
            }
        }

        Ok(results)
    }

    fn get_detailed_error(&self, return_code: SQLRETURN) -> SqlError {
        assert_ne!(return_code, SQL_SUCCESS);
        assert_ne!(return_code, SQL_SUCCESS_WITH_INFO);
        assert_ne!(return_code, SQL_STILL_EXECUTING);

        if let Ok(details) = self.diagnostics() {
            SqlError::OdbcError {
                return_code,
                details,
            }
        } else {
            return_code.into()
        }
    }
}

pub(crate) unsafe trait SqlRawHandle: SqlHandle {
    unsafe fn alloc_handle() -> SqlResult<SQLHANDLE> {
        let parent_handle: SQLHANDLE = std::ptr::null_mut();
        let mut handle: SQLHANDLE = std::ptr::null_mut();
        let ret = SQLAllocHandle(Self::TYPE, parent_handle, &mut handle);
        match ret {
            SQL_SUCCESS | SQL_SUCCESS_WITH_INFO => Ok(handle),
            SQL_ERROR => Err(ret.into()),
            _ => panic!("Unexpected SQLAllocHandle return code: {:?}", ret),
        }
    }

    unsafe fn dealloc_handle(&mut self) -> SqlResult {
        let ret = SQLFreeHandle(Self::TYPE, self.handle());
        match ret {
            SQL_SUCCESS | SQL_SUCCESS_WITH_INFO => Ok(()),
            SQL_ERROR => {
                let e = self.get_detailed_error(ret);
                Err(e)
            }
            _ => panic!("Unexpected SQLFreeHandle return code: {:?}", ret),
        }
    }

    unsafe fn alloc_child_handle<T: SqlRawHandle>(&self) -> SqlResult<SQLHANDLE> {
        let mut handle: SQLHANDLE = std::ptr::null_mut();
        let ret = SQLAllocHandle(T::TYPE, self.handle(), &mut handle);
        match ret {
            SQL_SUCCESS | SQL_SUCCESS_WITH_INFO => Ok(handle),
            SQL_ERROR => {
                let e = self.get_detailed_error(ret);
                Err(e)
            }
            _ => panic!("Unexpected child SQLAllocHandle return code: {:?}", ret),
        }
    }
}
