use super::*;
use crate::SqlPod;

impl<T: SqlPod> SqlValue<()> for T {
    fn clean(&mut self, _context: &mut ()) -> SqlResult {
        *self = SqlDefault::default();
        Ok(())
    }

    fn bind_column(
        &mut self,
        column: &SqlColumn,
        statement: &mut SqlStatement,
        _context: &mut (),
        indicator: &mut SQLLEN,
    ) -> SqlResult {
        let ret = unsafe {
            SQLBindCol(
                statement.typed_handle(),
                column.col_number,
                T::C_DATA_TYPE,
                (self as *mut T) as SQLPOINTER,
                std::mem::size_of::<T>() as SQLLEN,
                indicator,
            )
        };
        check_bind_column_result(statement, ret)
    }
    fn get_data(
        &mut self,
        column: &SqlColumn,
        statement: &mut SqlStatement,
        _context: &mut (),
        indicator: &mut SQLLEN,
    ) -> SqlPoll {
        let ret = unsafe {
            SQLGetData(
                statement.typed_handle(),
                column.col_number,
                T::C_DATA_TYPE,
                (self as *mut T) as SQLPOINTER,
                std::mem::size_of::<T>() as SQLLEN,
                indicator,
            )
        };
        check_get_data_status_code(statement, ret)
    }

    fn bind_parameter(
        &mut self,
        parameter_number: SQLUSMALLINT,
        options: SqlBindParameterOptions,
        statement: &SqlStatement,
        _context: &mut (),
        indicator: &mut SQLLEN,
    ) -> SqlResult {
        *indicator = std::mem::size_of::<T>() as SQLLEN;
        let ret = unsafe {
            SQLBindParameter(
                statement.typed_handle(),
                parameter_number,
                SQL_PARAM_INPUT,
                T::C_DATA_TYPE,
                options.get_parameter_type::<T>(),
                options.precision,
                options.get_decimal_digits::<T>(),
                (self as *mut T) as SQLPOINTER,
                std::mem::size_of::<T>() as SQLLEN,
                indicator,
            )
        };
        check_bind_parameter_result(statement, ret)
    }

    fn flush_context(&mut self, _context: &mut (), indicator: &mut SQLLEN) -> SqlResult {
        if *indicator == SQL_NULL_DATA {
            *self = SqlDefault::default();
        }
        Ok(())
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn visit() {
        use odbc_futures_derive::Odbc;

        #[allow(dead_code)]
        #[derive(Default, Copy, Clone, Odbc)]
        struct Pod {
            a: u32,
            b: Option<u16>,
            c: Option<SqlDataType>,
            d: Option<bool>,
        }
    }
}
