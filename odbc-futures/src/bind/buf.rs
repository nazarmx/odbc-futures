use super::*;
use crate::util::{self, VecCapacityExt};
use crate::{SqlColumn, SqlType};
use futures::Async;
use std::ffi::CString;

impl SqlValue<()> for String {
    fn clean(&mut self, _context: &mut ()) -> SqlResult {
        self.clear();
        Ok(())
    }

    fn bind_column(
        &mut self,
        column: &SqlColumn,
        statement: &mut SqlStatement,
        _context: &mut (),
        indicator: &mut SQLLEN,
    ) -> SqlResult {
        bind_column(
            unsafe { self.as_mut_vec() },
            statement,
            column,
            indicator,
            CString::C_DATA_TYPE,
        )
    }

    fn get_data(
        &mut self,
        column: &SqlColumn,
        statement: &mut SqlStatement,
        _context: &mut (),
        indicator: &mut SQLLEN,
    ) -> SqlPoll {
        get_data(
            unsafe { self.as_mut_vec() },
            statement,
            column,
            indicator,
            CString::C_DATA_TYPE,
        )
    }

    fn bind_parameter(
        &mut self,
        parameter_number: SQLUSMALLINT,
        options: SqlBindParameterOptions,
        statement: &SqlStatement,
        _context: &mut (),
        indicator: &mut SQLLEN,
    ) -> SqlResult {
        let parameter_type = if options.parameter_type == SQL_UNKNOWN_TYPE {
            CString::SQL_DATA_TYPE
        } else {
            options.parameter_type
        };
        bind_parameter(
            unsafe { self.as_mut_vec() },
            statement,
            parameter_number,
            options,
            indicator,
            CString::C_DATA_TYPE,
            parameter_type,
        )
    }

    fn flush_context(&mut self, _context: &mut (), indicator: &mut SQLLEN) -> SqlResult {
        flush(unsafe { self.as_mut_vec() }, *indicator)
    }
}

impl SqlValue<Vec<u16>> for String {
    fn clean(&mut self, context: &mut Vec<u16>) -> SqlResult {
        self.clear();
        context.clear();
        Ok(())
    }

    fn bind_column(
        &mut self,
        column: &SqlColumn,
        statement: &mut SqlStatement,
        context: &mut Vec<u16>,
        indicator: &mut SQLLEN,
    ) -> SqlResult {
        bind_column(context, statement, column, indicator, String::C_DATA_TYPE)
    }

    fn get_data(
        &mut self,
        column: &SqlColumn,
        statement: &mut SqlStatement,
        context: &mut Vec<u16>,
        indicator: &mut SQLLEN,
    ) -> SqlPoll {
        get_data(context, statement, column, indicator, String::C_DATA_TYPE)
    }

    fn bind_parameter(
        &mut self,
        parameter_number: SQLUSMALLINT,
        options: SqlBindParameterOptions,
        statement: &SqlStatement,
        context: &mut Vec<u16>,
        indicator: &mut SQLLEN,
    ) -> SqlResult {
        context.clear();
        context.extend(self.encode_utf16());
        let parameter_type = options.get_parameter_type::<String>();
        bind_parameter(
            context,
            statement,
            parameter_number,
            options,
            indicator,
            String::C_DATA_TYPE,
            parameter_type,
        )
    }

    fn flush_context(&mut self, context: &mut Vec<u16>, indicator: &mut SQLLEN) -> SqlResult {
        flush(context, *indicator)?;
        *self = util::from_utf_16_null_terminated(context)?;
        context.clear();
        Ok(())
    }
}

impl SqlValue<Vec<u8>> for CString {
    fn clean(&mut self, context: &mut Vec<u8>) -> SqlResult {
        *self = Default::default();
        context.clear();
        Ok(())
    }

    fn bind_column(
        &mut self,
        column: &SqlColumn,
        statement: &mut SqlStatement,
        context: &mut Vec<u8>,
        indicator: &mut SQLLEN,
    ) -> SqlResult {
        bind_column(context, statement, column, indicator, CString::C_DATA_TYPE)
    }

    fn get_data(
        &mut self,
        column: &SqlColumn,
        statement: &mut SqlStatement,
        context: &mut Vec<u8>,
        indicator: &mut SQLLEN,
    ) -> SqlPoll {
        get_data(context, statement, column, indicator, CString::C_DATA_TYPE)
    }

    fn bind_parameter(
        &mut self,
        parameter_number: SQLUSMALLINT,
        options: SqlBindParameterOptions,
        statement: &SqlStatement,
        context: &mut Vec<u8>,
        indicator: &mut SQLLEN,
    ) -> SqlResult {
        let parameter_type = options.get_parameter_type::<CString>();
        bind_parameter(
            context,
            statement,
            parameter_number,
            options,
            indicator,
            CString::C_DATA_TYPE,
            parameter_type,
        )
    }

    fn flush_context(&mut self, context: &mut Vec<u8>, indicator: &mut SQLLEN) -> SqlResult {
        flush(context, *indicator)?;
        if let Some(&0) = context.last() {
            context.pop();
        }
        *self = CString::new(&context[..])?;
        context.clear();
        Ok(())
    }
}

macro_rules! vec_impl {
    ($t:ty) => {
        impl SqlValue<()> for Vec<$t> {
            fn clean(&mut self, _context: &mut ()) -> SqlResult {
                self.clear();
                Ok(())
            }

            fn bind_column(
                &mut self,
                column: &SqlColumn,
                statement: &mut SqlStatement,
                _context: &mut (),
                indicator: &mut SQLLEN,
            ) -> SqlResult {
                bind_column(self, statement, column, indicator, <Vec<$t>>::C_DATA_TYPE)
            }

            fn get_data(
                &mut self,
                column: &SqlColumn,
                statement: &mut SqlStatement,
                _context: &mut (),
                indicator: &mut SQLLEN,
            ) -> SqlPoll {
                get_data(self, statement, column, indicator, <Vec<$t>>::C_DATA_TYPE)
            }

            fn bind_parameter(
                &mut self,
                parameter_number: SQLUSMALLINT,
                options: SqlBindParameterOptions,
                statement: &SqlStatement,
                _context: &mut (),
                indicator: &mut SQLLEN,
            ) -> SqlResult {
                let parameter_type = options.get_parameter_type::<Self>();
                bind_parameter(
                    self,
                    statement,
                    parameter_number,
                    options,
                    indicator,
                    <Vec<$t>>::C_DATA_TYPE,
                    parameter_type,
                )
            }

            fn flush_context(&mut self, _context: &mut (), indicator: &mut SQLLEN) -> SqlResult {
                flush(self, *indicator)
            }
        }
    };
}

vec_impl!(u8);
vec_impl!(u16);

fn bind_parameter<T>(
    data: &mut Vec<T>,
    statement: &SqlStatement,
    parameter_number: SQLUSMALLINT,
    options: SqlBindParameterOptions,
    indicator: &mut SQLLEN,
    value_type: SqlCDataType,
    parameter_type: SqlDataType,
) -> SqlResult {
    *indicator = std::mem::size_of_val(&data[..]) as SQLLEN;
    let column_size = if data.len() <= max_column_size::<T>() {
        data.len() as SQLULEN
    } else {
        0
    };

    let ret = unsafe {
        SQLBindParameter(
            statement.typed_handle(),
            parameter_number,
            options.input_output_type,
            value_type,
            parameter_type,
            column_size,
            crate::util::decimal_digits(parameter_type),
            data.as_mut_ptr() as SQLPOINTER,
            data.capacity_bytes() as SQLLEN,
            indicator,
        )
    };

    check_bind_parameter_result(statement, ret)
}

fn bind_column<T>(
    data: &mut Vec<T>,
    statement: &mut SqlStatement,
    column: &SqlColumn,
    len: &mut SQLLEN,
    data_type: SqlCDataType,
) -> SqlResult {
    debug_assert!(column.col_size > 0, "{:?}", column);
    update_capacity(
        data,
        column.col_size as usize,
        null_terminator_len(data_type),
    );
    let ret = unsafe {
        SQLBindCol(
            statement.typed_handle(),
            column.col_number,
            data_type,
            data.as_mut_ptr() as SQLPOINTER,
            data.capacity_bytes() as SQLLEN,
            len,
        )
    };
    check_bind_column_result(statement, ret)
}

fn flush<T>(data: &mut Vec<T>, len: SQLLEN) -> SqlResult {
    match expected_len::<T>(len) {
        Some(len) => unsafe { data.set_len_checked(len) },
        None => data.clear(),
    }
    Ok(())
}

fn get_data<T>(
    data: &mut Vec<T>,
    statement: &mut SqlStatement,
    column: &SqlColumn,
    len: &mut SQLLEN,
    data_type: SqlCDataType,
) -> SqlPoll {
    let col_number = column.col_number;
    let null_terminator_len = null_terminator_len(data_type);
    update_capacity(data, column.col_size as usize, null_terminator_len);

    loop {
        let ret = unsafe {
            SQLGetData(
                statement.typed_handle(),
                col_number,
                data_type,
                data.as_mut_ptr().add(data.len()) as SQLPOINTER,
                data.remaining_capacity_bytes() as SQLLEN,
                len,
            )
        };

        match ret {
            SQL_SUCCESS | SQL_NO_DATA | SQL_SUCCESS_WITH_INFO => {
                if util::is_string_data_right_truncated(statement, ret)? {
                    // buffer is fully filled up
                    let capacity = data.capacity();
                    debug_assert!(capacity >= null_terminator_len);
                    unsafe {
                        data.set_len_checked(capacity - null_terminator_len);
                    }

                    match expected_len::<T>(*len) {
                        Some(len) => update_capacity(data, len, null_terminator_len),
                        None => data.reserve(max_column_size::<T>() + null_terminator_len),
                    };
                } else {
                    if *len == SQL_NULL_DATA {
                        data.clear();
                    } else {
                        assert!(*len >= 0);
                        *len += std::mem::size_of_val(data.as_slice()) as SQLLEN;
                        // we got the last chunk of data
                        unsafe { data.set_len_checked(expected_len::<T>(*len).unwrap()) }
                    }
                    return Ok(Async::Ready(()));
                }
            }
            SQL_STILL_EXECUTING => return Ok(Async::NotReady),
            SQL_ERROR => {
                let e = statement.get_detailed_error(ret);
                return Err(e);
            }
            _ => panic!("Unexpected SQLGetData return code: {:?}", ret),
        }
    }
}

fn max_column_size<T>() -> usize {
    const MAX_COL_SIZE_BYTES: usize = 8000;
    let size = std::mem::size_of::<T>();
    debug_assert_eq!(0, MAX_COL_SIZE_BYTES % size);
    MAX_COL_SIZE_BYTES / size
}

fn null_terminator_len(data_type: SqlCDataType) -> usize {
    match data_type {
        SQL_C_BINARY => 0,
        SQL_C_CHAR | SQL_C_WCHAR => 1,
        _ => panic!("Unexpected C data type: {:?}", data_type),
    }
}

fn expected_len<T>(len: SQLLEN) -> Option<usize> {
    if len >= 0 {
        let len = len as usize;
        let size = std::mem::size_of::<T>();
        debug_assert_eq!(0, len % size);
        Some(len / size)
    } else {
        None
    }
}

fn update_capacity<T>(data: &mut Vec<T>, col_size: usize, null_terminator_len: usize) {
    if col_size > 0 {
        data.reserve_capacity(col_size + null_terminator_len);
    } else {
        data.reserve_capacity(max_column_size::<T>() + null_terminator_len);
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn visit() {
        use odbc_futures_derive::Odbc;

        #[allow(dead_code)]
        #[derive(Default, Clone, Odbc)]
        struct Buf {
            a: Vec<u16>,
            #[odbc_string_utf8]
            b: Option<String>,
            c: Option<Vec<u8>>,
            d: Vec<u8>,
            e: String,
            f: CString,
            g: Option<CString>,
        }
    }
}
