use super::*;
use crate::SqlType;
use uuid::Uuid;

impl SqlValue<SQLGUID> for Uuid {
    fn clean(&mut self, context: &mut SQLGUID) -> SqlResult {
        *self = SqlDefault::default();
        *context = Default::default();
        Ok(())
    }

    fn bind_column(
        &mut self,
        column: &SqlColumn,
        statement: &mut SqlStatement,
        context: &mut SQLGUID,
        indicator: &mut SQLLEN,
    ) -> SqlResult {
        let ret = unsafe {
            SQLBindCol(
                statement.typed_handle(),
                column.col_number,
                Uuid::C_DATA_TYPE,
                (context as *mut SQLGUID) as SQLPOINTER,
                std::mem::size_of::<SQLGUID>() as SQLLEN,
                indicator,
            )
        };
        check_bind_column_result(statement, ret)
    }
    fn get_data(
        &mut self,
        column: &SqlColumn,
        statement: &mut SqlStatement,
        context: &mut SQLGUID,
        indicator: &mut SQLLEN,
    ) -> SqlPoll {
        let ret = unsafe {
            SQLGetData(
                statement.typed_handle(),
                column.col_number,
                Uuid::C_DATA_TYPE,
                (context as *mut SQLGUID) as SQLPOINTER,
                std::mem::size_of::<SQLGUID>() as SQLLEN,
                indicator,
            )
        };
        check_get_data_status_code(statement, ret)
    }

    fn bind_parameter(
        &mut self,
        parameter_number: SQLUSMALLINT,
        options: SqlBindParameterOptions,
        statement: &SqlStatement,
        context: &mut SQLGUID,
        indicator: &mut SQLLEN,
    ) -> SqlResult {
        *indicator = std::mem::size_of::<SQLGUID>() as SQLLEN;
        let (d1, d2, d3, d4) = self.as_fields();
        *context = SQLGUID {
            d1,
            d2,
            d3,
            d4: *d4,
        };

        let ret = unsafe {
            SQLBindParameter(
                statement.typed_handle(),
                parameter_number,
                SQL_PARAM_INPUT,
                Uuid::C_DATA_TYPE,
                options.get_parameter_type::<Uuid>(),
                options.precision,
                options.get_decimal_digits::<Uuid>(),
                (context as *mut SQLGUID) as SQLPOINTER,
                std::mem::size_of::<SQLGUID>() as SQLLEN,
                indicator,
            )
        };
        check_bind_parameter_result(statement, ret)
    }

    fn flush_context(&mut self, context: &mut SQLGUID, indicator: &mut SQLLEN) -> SqlResult {
        if *indicator == SQL_NULL_DATA {
            *self = SqlDefault::default();
        } else {
            *self = Uuid::from_fields(context.d1, context.d2, context.d3, &context.d4[..])?;
        }
        Ok(())
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn visit() {
        use odbc_futures_derive::Odbc;

        #[allow(dead_code)]
        #[derive(Default, Copy, Clone, Odbc)]
        struct UuidTest {
            a: Uuid,
            b: Option<Uuid>,
        }
    }
}
