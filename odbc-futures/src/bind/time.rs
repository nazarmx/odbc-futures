use super::*;
use crate::util;
use chrono::NaiveTime;

impl SqlValue<SQL_TIME_STRUCT> for NaiveTime {
    fn clean(&mut self, context: &mut SQL_TIME_STRUCT) -> SqlResult {
        *self = SqlDefault::default();
        *context = Default::default();
        Ok(())
    }

    fn bind_column(
        &mut self,
        column: &SqlColumn,
        statement: &mut SqlStatement,
        context: &mut SQL_TIME_STRUCT,
        indicator: &mut SQLLEN,
    ) -> SqlResult {
        let ret = unsafe {
            SQLBindCol(
                statement.typed_handle(),
                column.col_number,
                NaiveTime::C_DATA_TYPE,
                (context as *mut SQL_TIME_STRUCT) as SQLPOINTER,
                std::mem::size_of::<SQL_TIME_STRUCT>() as SQLLEN,
                indicator,
            )
        };
        check_bind_column_result(statement, ret)
    }
    fn get_data(
        &mut self,
        column: &SqlColumn,
        statement: &mut SqlStatement,
        context: &mut SQL_TIME_STRUCT,
        indicator: &mut SQLLEN,
    ) -> SqlPoll {
        let ret = unsafe {
            SQLGetData(
                statement.typed_handle(),
                column.col_number,
                NaiveTime::C_DATA_TYPE,
                (context as *mut SQL_TIME_STRUCT) as SQLPOINTER,
                std::mem::size_of::<SQL_TIME_STRUCT>() as SQLLEN,
                indicator,
            )
        };
        check_get_data_status_code(statement, ret)
    }

    fn bind_parameter(
        &mut self,
        parameter_number: SQLUSMALLINT,
        options: SqlBindParameterOptions,
        statement: &SqlStatement,
        context: &mut SQL_TIME_STRUCT,
        indicator: &mut SQLLEN,
    ) -> SqlResult {
        use chrono::Timelike;
        let hour = self.hour();
        assert!(hour >= std::u16::MIN.into() && hour <= std::u16::MAX.into());
        let minute = self.minute();
        assert!(minute >= std::u16::MIN.into() && minute <= std::u16::MAX.into());
        let second = self.second();
        assert!(second >= std::u16::MIN.into() && second <= std::u16::MAX.into());

        *context = SQL_TIME_STRUCT {
            hour: hour as SQLUSMALLINT,
            minute: minute as SQLUSMALLINT,
            second: second as SQLUSMALLINT,
        };

        let ret = unsafe {
            SQLBindParameter(
                statement.typed_handle(),
                parameter_number,
                SQL_PARAM_INPUT,
                NaiveTime::C_DATA_TYPE,
                options.get_parameter_type::<NaiveTime>(),
                options.precision,
                options.get_decimal_digits::<NaiveTime>(),
                (context as *mut SQL_TIME_STRUCT) as SQLPOINTER,
                std::mem::size_of::<SQL_TIME_STRUCT>() as SQLLEN,
                indicator,
            )
        };
        check_bind_parameter_result(statement, ret)
    }

    fn flush_context(
        &mut self,
        context: &mut SQL_TIME_STRUCT,
        indicator: &mut SQLLEN,
    ) -> SqlResult {
        if *indicator == SQL_NULL_DATA {
            *self = SqlDefault::default();
        } else {
            *self = NaiveTime::from_hms(
                u32::from(context.hour),
                u32::from(context.minute),
                u32::from(context.second),
            );
        }
        Ok(())
    }
}

impl SqlValue<SQL_SS_TIME2_STRUCT> for NaiveTime {
    fn clean(&mut self, context: &mut SQL_SS_TIME2_STRUCT) -> SqlResult {
        *self = SqlDefault::default();
        *context = Default::default();
        Ok(())
    }

    fn bind_column(
        &mut self,
        column: &SqlColumn,
        statement: &mut SqlStatement,
        context: &mut SQL_SS_TIME2_STRUCT,
        indicator: &mut SQLLEN,
    ) -> SqlResult {
        let ret = unsafe {
            SQLBindCol(
                statement.typed_handle(),
                column.col_number,
                SQL_C_SS_TIME2,
                (context as *mut SQL_SS_TIME2_STRUCT) as SQLPOINTER,
                std::mem::size_of::<SQL_SS_TIME2_STRUCT>() as SQLLEN,
                indicator,
            )
        };
        check_bind_column_result(statement, ret)
    }
    fn get_data(
        &mut self,
        column: &SqlColumn,
        statement: &mut SqlStatement,
        context: &mut SQL_SS_TIME2_STRUCT,
        indicator: &mut SQLLEN,
    ) -> SqlPoll {
        let ret = unsafe {
            SQLGetData(
                statement.typed_handle(),
                column.col_number,
                SQL_C_SS_TIME2,
                (context as *mut SQL_SS_TIME2_STRUCT) as SQLPOINTER,
                std::mem::size_of::<SQL_SS_TIME2_STRUCT>() as SQLLEN,
                indicator,
            )
        };
        check_get_data_status_code(statement, ret)
    }

    fn bind_parameter(
        &mut self,
        parameter_number: SQLUSMALLINT,
        options: SqlBindParameterOptions,
        statement: &SqlStatement,
        context: &mut SQL_SS_TIME2_STRUCT,
        indicator: &mut SQLLEN,
    ) -> SqlResult {
        *indicator = std::mem::size_of::<SQL_SS_TIME2_STRUCT>() as SQLLEN;
        use chrono::Timelike;
        let hour = self.hour();
        assert!(hour >= std::u16::MIN.into() && hour <= std::u16::MAX.into());
        let minute = self.minute();
        assert!(minute >= std::u16::MIN.into() && minute <= std::u16::MAX.into());
        let second = self.second();
        assert!(second >= std::u16::MIN.into() && second <= std::u16::MAX.into());
        let fraction = self.nanosecond();

        let decimal_digits = options.get_decimal_digits::<NaiveTime>();

        *context = SQL_SS_TIME2_STRUCT {
            hour: hour as SQLUSMALLINT,
            minute: minute as SQLUSMALLINT,
            second: second as SQLUSMALLINT,
            fraction: util::truncate_nanoseconds(fraction as SQLUINTEGER, decimal_digits),
        };

        let ret = unsafe {
            SQLBindParameter(
                statement.typed_handle(),
                parameter_number,
                SQL_PARAM_INPUT,
                SQL_C_SS_TIME2,
                options.get_parameter_type::<NaiveTime>(),
                options.precision,
                decimal_digits,
                (context as *mut SQL_SS_TIME2_STRUCT) as SQLPOINTER,
                std::mem::size_of::<SQL_SS_TIME2_STRUCT>() as SQLLEN,
                indicator,
            )
        };
        check_bind_parameter_result(statement, ret)
    }

    fn flush_context(
        &mut self,
        context: &mut SQL_SS_TIME2_STRUCT,
        indicator: &mut SQLLEN,
    ) -> SqlResult {
        if *indicator == SQL_NULL_DATA {
            *self = SqlDefault::default();
        } else {
            *self = NaiveTime::from_hms_nano(
                u32::from(context.hour),
                u32::from(context.minute),
                u32::from(context.second),
                u32::from(context.fraction),
            );
        }
        Ok(())
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn visit() {
        use odbc_futures_derive::Odbc;

        #[allow(dead_code)]
        #[derive(Copy, Clone, Odbc)]
        struct Time {
            #[odbc_time = "time"]
            a: NaiveTime,
            #[odbc_time = "time"]
            b: Option<NaiveTime>,
            #[odbc_time = "time2"]
            c: NaiveTime,
            #[odbc_time = "time2"]
            d: Option<NaiveTime>,
        }

        impl Default for Time {
            fn default() -> Time {
                Time {
                    a: SqlDefault::default(),
                    b: None,
                    c: SqlDefault::default(),
                    d: None,
                }
            }
        }
    }
}
