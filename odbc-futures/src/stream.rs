use crate::bind::*;
use crate::error::*;
use crate::handle::*;
use crate::poll::*;
use crate::stmt::*;
use crate::util;
use crate::util::VecCapacityExt;
use futures::Async;
use odbc_sys::*;

#[derive(Debug, Clone)]
pub struct SqlColumn {
    pub col_name: String,
    pub data_type: SqlDataType,
    pub col_number: SQLUSMALLINT,
    pub col_size: SQLULEN,
    pub decimal_digits: SQLSMALLINT,
    pub nullable: Nullable,
}

pub(crate) fn num_result_cols(statement: &mut SqlStatement) -> SqlPoll<SQLSMALLINT> {
    let mut num_result_cols: SQLSMALLINT = 0;
    let ret = unsafe { SQLNumResultCols(statement.typed_handle(), &mut num_result_cols) };

    match ret {
        SQL_SUCCESS | SQL_SUCCESS_WITH_INFO => Ok(Async::Ready(num_result_cols)),
        SQL_STILL_EXECUTING => Ok(Async::NotReady),
        SQL_ERROR => Err(statement.get_detailed_error(ret)),
        _ => panic!("Unexpected SQLNumResultCols return code: {:?}", ret),
    }
}

pub(crate) fn describe_cols_stream(
    statement: SqlStatement,
    num_result_cols: SQLSMALLINT,
) -> SqlStatementStream<SqlColumn> {
    assert!(num_result_cols >= 0);
    let num_result_cols = num_result_cols as SQLUSMALLINT;

    let mut col_number: SQLUSMALLINT = 1;
    let mut col_name: Vec<u16> = Vec::with_capacity(SQL_MAX_MESSAGE_LENGTH as usize);
    let mut name_length: SQLSMALLINT = 0;
    let mut data_type: SqlDataType = SQL_UNKNOWN_TYPE;
    let mut col_size: SQLULEN = 0;
    let mut decimal_digits: SQLSMALLINT = 0;
    let mut nullable = SQL_NULLABLE_UNKNOWN;

    SqlStatementStream::new(statement, false, move |statement| {
        if col_number <= num_result_cols {
            loop {
                let ret = unsafe {
                    SQLDescribeColW(
                        statement.typed_handle(),
                        col_number,
                        col_name.as_mut_ptr(),
                        col_name.capacity() as SQLSMALLINT,
                        &mut name_length,
                        &mut data_type,
                        &mut col_size,
                        &mut decimal_digits,
                        &mut nullable,
                    )
                };

                match ret {
                    SQL_SUCCESS | SQL_SUCCESS_WITH_INFO => {
                        assert!(name_length >= 0);
                        let name_length = name_length as usize;

                        if util::is_string_data_right_truncated(statement, ret)? {
                            col_name.reserve_capacity(name_length + 1);
                        } else {
                            unsafe {
                                col_name.set_len_checked(name_length);
                            }
                            let col_name = util::from_utf_16_null_terminated(&col_name)?;
                            let column = SqlColumn {
                                col_name,
                                data_type,
                                col_number,
                                nullable,
                                col_size,
                                decimal_digits,
                            };
                            col_number += 1;
                            return Ok(Async::Ready(Some(column)));
                        }
                    }
                    SQL_STILL_EXECUTING => break,
                    SQL_ERROR => return Err(statement.get_detailed_error(ret)),
                    _ => panic!("Unexpected SQLDescribeColW return code: {:?}", ret),
                }
            }
            Ok(Async::NotReady)
        } else {
            Ok(Async::Ready(None))
        }
    })
}

pub(crate) fn fetch_state_stream<T: SqlColumns>(
    columns: Vec<SqlColumn>,
    statement: SqlStatement,
) -> Result<SqlStatementStream<T>, (SqlError, SqlStatement)> {
    use crate::bind::SqlVisitor;

    let mut visitor = match SqlVisitor::new(&statement, columns) {
        Ok(visitor) => visitor,
        Err(e) => return Err((e, statement)),
    };
    let mut need_bind = true;
    let mut get_data = false;
    let mut value = T::default();

    let stream = SqlStatementStream::new(statement, true, move |statement| {
        if need_bind {
            visitor.clean(statement, &mut value)?;
            visitor.bind(statement, &mut value)?;
            need_bind = false;
        }

        loop {
            if get_data {
                match visitor.get_data(statement, &mut value) {
                    Ok(x) => match x {
                        Async::Ready(_) => {
                            get_data = false;
                            visitor.flush_context(statement, &mut value)?;
                            let mut val = value.clone();
                            visitor.clean(statement, &mut value)?;
                            visitor.flush_optional(statement, &mut val)?;
                            return Ok(Async::Ready(Some(val)));
                        }
                        _ => break,
                    },
                    Err(e) => return Err(e),
                }
            } else {
                let ret = unsafe { SQLFetch(statement.typed_handle()) };

                match ret {
                    SQL_SUCCESS | SQL_SUCCESS_WITH_INFO => {
                        //assert!(!util::is_string_data_right_truncated(statement, ret)?);
                        get_data = true;
                    }
                    SQL_STILL_EXECUTING => break,
                    SQL_NO_DATA => return Ok(Async::Ready(None)),
                    SQL_ERROR => return Err(statement.get_detailed_error(ret)),
                    _ => panic!("Unexpected SQLFetchScroll return code: {:?}", ret),
                }
            }
        }

        Ok(Async::NotReady)
    });

    Ok(stream)
}
