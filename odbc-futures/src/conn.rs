use crate::attr::*;
use crate::env::*;
use crate::error::*;
use crate::handle::*;
use crate::poll::*;
use crate::tran::*;
use crate::util;
use bitflags::bitflags;
use futures::*;
use odbc_sys::*;
use std::sync::{Arc, RwLock};

#[derive(Debug)]
pub struct SqlConnection {
    handle: SQLHDBC,
    environment: Arc<RwLock<SqlEnvironment>>,
    is_connected: bool,
}

impl Drop for SqlConnection {
    fn drop(&mut self) {
        if self.is_connected {
            loop {
                let ret = unsafe { SQLDisconnect(self.handle) };
                match ret {
                    SQL_SUCCESS | SQL_SUCCESS_WITH_INFO => break,
                    SQL_STILL_EXECUTING => continue,
                    SQL_ERROR => {
                        let e = self.get_detailed_error(ret);
                        panic!("{}", e)
                    }
                    _ => panic!("Unexpected SQLDisconnect return code: {:?}", ret),
                };
            }
        }
        unsafe { self.dealloc_handle().unwrap() }
    }
}

impl SqlHandle for SqlConnection {
    const TYPE: HandleType = SQL_HANDLE_DBC;
    type Type = SQLHDBC;

    unsafe fn typed_handle(&self) -> SQLHDBC {
        self.handle
    }

    unsafe fn handle(&self) -> SQLHANDLE {
        self.handle as SQLHANDLE
    }
}

impl SqlAttribute for SqlConnectionAttribute {
    fn buffer_length(&self) -> Option<SqlAttributeStringLength> {
        match self {
            SQL_ATTR_TRACEFILE | SQL_ATTR_TRANSLATE_LIB | SQL_ATTR_CURRENT_CATALOG => None,
            SQL_ATTR_TRANSLATE_OPTION | SQL_ATTR_TXN_ISOLATION => Some(SQL_IS_INTEGER),
            SQL_ATTR_QUIET_MODE | SQL_ATTR_ENLIST_IN_DTC => Some(SQL_IS_POINTER),
            _ => Some(SQL_IS_UINTEGER),
        }
    }
}

unsafe impl SqlAttributes for SqlConnection {
    type AttributeType = SqlConnectionAttribute;

    const GETTER_NAME: &'static str = "SQLGetConnectAttrW";
    const GETTER: unsafe extern "system" fn(
        SQLHDBC,
        SqlConnectionAttribute,
        SQLPOINTER,
        SQLINTEGER,
        *mut SQLINTEGER,
    ) -> SQLRETURN = SQLGetConnectAttrW;
    const SETTER: unsafe extern "system" fn(
        SQLHDBC,
        SqlConnectionAttribute,
        SQLPOINTER,
        SQLINTEGER,
    ) -> SQLRETURN = SQLSetConnectAttrW;
    const SETTER_NAME: &'static str = "SQLSetConnectAttrW";
}

unsafe impl Send for SqlConnection {}

unsafe impl Sync for SqlConnection {}

unsafe impl SqlRawHandle for SqlConnection {}

impl SqlEndTransaction for SqlConnection {}

impl SqlConnection {
    pub fn new(environment: &Arc<RwLock<SqlEnvironment>>) -> SqlResult<SqlConnection> {
        let environment = Arc::clone(environment);
        let handle = {
            let lock = environment.read().unwrap();
            unsafe { lock.alloc_child_handle::<SqlConnection>()? as SQLHDBC }
        };
        Ok(SqlConnection {
            handle,
            environment,
            is_connected: false,
        })
    }

    pub fn environment(&self) -> Arc<RwLock<SqlEnvironment>> {
        Arc::clone(&self.environment)
    }

    pub fn set_async(&mut self, is_async: bool) -> SqlResult {
        unsafe {
            self.set_attribute(
                SqlConnectionAttribute::SQL_ATTR_ASYNC_ENABLE,
                is_async as usize,
            )
        }
    }

    pub fn get_async(&self) -> SqlResult<bool> {
        unsafe {
            self.get_attribute::<SQLUINTEGER>(SqlConnectionAttribute::SQL_ATTR_ASYNC_ENABLE)
                .map(util::u32_to_bool)
        }
    }

    pub fn set_connect_async(&mut self, is_async: bool) -> SqlResult {
        unsafe { self.set_attribute(SQL_ATTR_ASYNC_DBC_FUNCTIONS_ENABLE, is_async as usize) }
    }

    pub fn get_connect_async(&self) -> SqlResult<bool> {
        unsafe {
            self.get_attribute::<SQLUINTEGER>(SQL_ATTR_ASYNC_DBC_FUNCTIONS_ENABLE)
                .map(util::u32_to_bool)
        }
    }

    pub fn connect_dsn(&mut self, dsn: &str) -> SqlResult {
        let raw_dsn: Vec<u16> = dsn.encode_utf16().collect();
        self.connect_dsn_impl(&raw_dsn).unwrap_async()
    }

    pub fn connect_dsn_async(self, dsn: &str) -> SqlFuture<SqlConnection> {
        let raw_dsn: Vec<u16> = dsn.encode_utf16().collect();
        let f = SqlValueFuture::new(self, move |connection: &mut SqlConnection| {
            connection.connect_dsn_impl(&raw_dsn)
        });

        flatten_value_future(f)
    }

    fn connect_dsn_impl(&mut self, raw_dsn: &[u16]) -> SqlPoll {
        let ret = unsafe {
            SQLConnectW(
                self.handle,
                raw_dsn.as_ptr(),
                raw_dsn.len() as SQLSMALLINT,
                std::ptr::null(),
                0,
                std::ptr::null(),
                0,
            )
        };

        match ret {
            SQL_SUCCESS | SQL_SUCCESS_WITH_INFO => {
                self.is_connected = true;
                Ok(Async::Ready(()))
            }
            SQL_STILL_EXECUTING => Ok(Async::NotReady),
            SQL_NO_DATA | SQL_ERROR => {
                let e = self.get_detailed_error(ret);
                Err(e)
            }
            _ => panic!("Unexpected SQLConnectW return code: {:?}", ret),
        }
    }

    pub fn driver_connect(&mut self, connection_string: &str) -> SqlResult {
        let raw_connection_string: Vec<u16> = connection_string.encode_utf16().collect();
        self.driver_connect_impl(&raw_connection_string)
            .unwrap_async()
    }

    pub fn driver_connect_async(self, connection_string: &str) -> SqlFuture<SqlConnection> {
        let raw_connection_string: Vec<u16> = connection_string.encode_utf16().collect();

        let f = SqlValueFuture::new(self, move |connection: &mut SqlConnection| {
            connection.driver_connect_impl(&raw_connection_string)
        });

        flatten_value_future(f)
    }

    fn driver_connect_impl(&mut self, raw_connection_string: &[u16]) -> SqlPoll {
        let ret = unsafe {
            SQLDriverConnectW(
                self.handle,
                std::ptr::null_mut(),
                raw_connection_string.as_ptr(),
                raw_connection_string.len() as SQLSMALLINT,
                std::ptr::null_mut(),
                0,
                std::ptr::null_mut(),
                SQL_DRIVER_NOPROMPT,
            )
        };

        match ret {
            SQL_SUCCESS | SQL_SUCCESS_WITH_INFO => {
                self.is_connected = true;
                Ok(Async::Ready(()))
            }
            SQL_STILL_EXECUTING => Ok(Async::NotReady),
            SQL_NO_DATA | SQL_ERROR => {
                let e = self.get_detailed_error(ret);
                Err(e)
            }
            _ => panic!("Unexpected SQLDriverConnectW return code: {:?}", ret),
        }
    }

    pub fn get_read_only(&self) -> SqlResult<bool> {
        unsafe {
            self.get_attribute::<SQLUINTEGER>(SQL_ATTR_ACCESS_MODE)
                .map(util::u32_to_bool)
        }
    }

    pub fn get_read_only_async(
        self,
    ) -> impl Future<Item = (SqlConnection, bool), Error = (SqlError, SqlConnection)> {
        unsafe {
            self.get_attribute_async::<SQLUINTEGER>(SQL_ATTR_ACCESS_MODE)
                .map(|(connection, value)| (connection, util::u32_to_bool(value)))
        }
    }

    pub fn set_read_only(&mut self, read_only: bool) -> SqlResult {
        unsafe { self.set_attribute(SQL_ATTR_ACCESS_MODE, read_only as usize) }
    }

    pub fn set_read_only_async(self, read_only: bool) -> SqlFuture<SqlConnection> {
        unsafe { self.set_attribute_async(SQL_ATTR_ACCESS_MODE, read_only as usize) }
    }

    pub fn get_autocommit(&self) -> SqlResult<bool> {
        unsafe {
            self.get_attribute::<SQLUINTEGER>(SQL_ATTR_AUTOCOMMIT)
                .map(util::u32_to_bool)
        }
    }

    pub fn get_autocommit_async(
        self,
    ) -> impl Future<Item = (SqlConnection, bool), Error = (SqlError, SqlConnection)> {
        unsafe {
            self.get_attribute_async::<SQLUINTEGER>(SQL_ATTR_AUTOCOMMIT)
                .map(|(connection, value)| (connection, util::u32_to_bool(value)))
        }
    }

    pub fn set_autocommit(&mut self, autocommit: bool) -> SqlResult {
        unsafe { self.set_attribute(SQL_ATTR_AUTOCOMMIT, autocommit as usize) }
    }

    pub fn set_autocommit_async(self, autocommit: bool) -> SqlFuture<SqlConnection> {
        unsafe { self.set_attribute_async(SQL_ATTR_AUTOCOMMIT, autocommit as usize) }
    }

    pub fn get_dead(&self) -> SqlResult<bool> {
        unsafe {
            self.get_attribute::<SQLUINTEGER>(SQL_ATTR_CONNECTION_DEAD)
                .map(util::u32_to_bool)
        }
    }

    pub fn get_dead_async(
        self,
    ) -> impl Future<Item = (SqlConnection, bool), Error = (SqlError, SqlConnection)> {
        unsafe {
            self.get_attribute_async::<SQLUINTEGER>(SQL_ATTR_CONNECTION_DEAD)
                .map(|(connection, value)| (connection, util::u32_to_bool(value)))
        }
    }

    pub fn get_connection_timeout(&self) -> SqlResult<u32> {
        unsafe { self.get_attribute::<SQLUINTEGER>(SQL_ATTR_CONNECTION_TIMEOUT) }
    }

    pub fn get_connection_timeout_async(self) -> SqlValueFuture<SqlConnection, u32> {
        unsafe { self.get_attribute_async::<SQLUINTEGER>(SQL_ATTR_CONNECTION_TIMEOUT) }
    }

    pub fn set_connection_timeout(&mut self, timeout_s: u32) -> SqlResult {
        unsafe { self.set_attribute(SQL_ATTR_CONNECTION_TIMEOUT, timeout_s as usize) }
    }

    pub fn set_connection_timeout_async(self, timeout_s: u32) -> SqlFuture<SqlConnection> {
        unsafe { self.set_attribute_async(SQL_ATTR_CONNECTION_TIMEOUT, timeout_s as usize) }
    }

    pub fn get_current_catalog(&self) -> SqlResult<String> {
        unsafe { self.get_attribute_string(SQL_ATTR_CURRENT_CATALOG) }
    }

    pub fn get_current_catalog_async(self) -> SqlValueFuture<SqlConnection, String> {
        unsafe { self.get_attribute_string_async(SQL_ATTR_CURRENT_CATALOG) }
    }

    pub fn set_current_catalog(&mut self, catalog: &str) -> SqlResult {
        unsafe { self.set_attribute_string(SQL_ATTR_CURRENT_CATALOG, catalog) }
    }

    pub fn set_current_catalog_async(self, catalog: &str) -> SqlFuture<SqlConnection> {
        unsafe { self.set_attribute_string_async(SQL_ATTR_CURRENT_CATALOG, catalog) }
    }

    pub fn get_login_timeout(&self) -> SqlResult<u32> {
        unsafe { self.get_attribute::<SQLUINTEGER>(SQL_ATTR_LOGIN_TIMEOUT) }
    }

    pub fn get_login_timeout_async(self) -> SqlValueFuture<SqlConnection, u32> {
        unsafe { self.get_attribute_async::<SQLUINTEGER>(SQL_ATTR_LOGIN_TIMEOUT) }
    }

    pub fn set_login_timeout(&mut self, timeout_s: u32) -> SqlResult {
        unsafe { self.set_attribute(SQL_ATTR_LOGIN_TIMEOUT, timeout_s as usize) }
    }

    pub fn set_login_timeout_async(self, timeout_s: u32) -> SqlFuture<SqlConnection> {
        unsafe { self.set_attribute_async(SQL_ATTR_LOGIN_TIMEOUT, timeout_s as usize) }
    }

    pub fn get_metadata_id(&self) -> SqlResult<bool> {
        unsafe {
            self.get_attribute::<SQLUINTEGER>(SQL_ATTR_METADATA_ID)
                .map(util::u32_to_bool)
        }
    }

    pub fn get_metadata_id_async(
        self,
    ) -> impl Future<Item = (SqlConnection, bool), Error = (SqlError, SqlConnection)> {
        unsafe {
            self.get_attribute_async::<SQLUINTEGER>(SQL_ATTR_METADATA_ID)
                .map(|(connection, value)| (connection, util::u32_to_bool(value)))
        }
    }

    pub fn set_metadata_id(&mut self, metadata_id: bool) -> SqlResult {
        unsafe { self.set_attribute(SQL_ATTR_METADATA_ID, metadata_id as usize) }
    }

    pub fn set_metadata_id_async(self, metadata_id: bool) -> SqlFuture<SqlConnection> {
        unsafe { self.set_attribute_async(SQL_ATTR_METADATA_ID, metadata_id as usize) }
    }

    pub fn get_packet_size(&self) -> SqlResult<u32> {
        unsafe { self.get_attribute::<SQLUINTEGER>(SQL_ATTR_PACKET_SIZE) }
    }

    pub fn get_packet_size_async(self) -> SqlValueFuture<SqlConnection, u32> {
        unsafe { self.get_attribute_async::<SQLUINTEGER>(SQL_ATTR_PACKET_SIZE) }
    }

    pub fn set_packet_size(&mut self, packet_size: u32) -> SqlResult {
        unsafe { self.set_attribute(SQL_ATTR_PACKET_SIZE, packet_size as usize) }
    }

    pub fn set_packet_size_async(self, packet_size: u32) -> SqlFuture<SqlConnection> {
        unsafe { self.set_attribute_async(SQL_ATTR_PACKET_SIZE, packet_size as usize) }
    }

    unsafe fn dummy_connection() -> SqlConnection {
        SqlConnection {
            handle: std::ptr::null_mut(),
            is_connected: false,
            environment: std::mem::uninitialized(),
        }
    }

    pub fn get_trace() -> SqlResult<bool> {
        unsafe {
            let conn = Self::dummy_connection();
            let ok = conn
                .get_attribute::<SQLUINTEGER>(SQL_ATTR_TRACE)
                .map(util::u32_to_bool);
            std::mem::forget(conn);
            ok
        }
    }

    pub fn set_trace(trace: bool) -> SqlResult {
        unsafe {
            let mut conn = Self::dummy_connection();
            let ok = conn.set_attribute(SQL_ATTR_TRACE, trace as usize);
            std::mem::forget(conn);
            ok
        }
    }

    pub fn get_trace_file() -> SqlResult<String> {
        unsafe {
            let conn = Self::dummy_connection();
            let ok = conn.get_attribute_string(SQL_ATTR_TRACEFILE);
            std::mem::forget(conn);
            ok
        }
    }

    pub fn set_trace_file(trace_file: &str) -> SqlResult {
        unsafe {
            let mut conn = Self::dummy_connection();
            let ok = conn.set_attribute_string(SQL_ATTR_TRACEFILE, trace_file);
            std::mem::forget(conn);
            ok
        }
    }

    pub fn get_info_getdata_extensions(&self) -> SqlResult<SqlGetDataExtensions> {
        unsafe { self.get_info_impl(InfoType::SQL_GETDATA_EXTENSIONS) }
    }

    unsafe fn get_info_impl<T>(&self, info_type: InfoType) -> SqlResult<T>
    where
        T: Copy + 'static,
    {
        let mut value: T = std::mem::uninitialized();

        let ret = SQLGetInfoW(
            self.handle,
            info_type,
            (&mut value as *mut T) as SQLPOINTER,
            0,
            std::ptr::null_mut(),
        );

        match ret {
            SQL_SUCCESS | SQL_SUCCESS_WITH_INFO => Ok(value),
            SQL_ERROR => {
                let e = self.get_detailed_error(ret);
                Err(e)
            }
            _ => panic!("Unexpected SQLGetInfoW return code: {:?}", ret),
        }
    }
}

bitflags! {
    pub struct SqlGetDataExtensions: u32 {
        const ANY_COLUMN = 0x0000_0001;
        const ANY_ORDER = 0x0000_0002;
        const BLOCK = 0x0000_0004;
        const BOUND = 0x0000_0008;
        const OUTPUT_PARAMS = 0x0000_0010;
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::util::{print_diagnostics, print_diagnostics_async};
    use futures::Future;
    use tokio;

    #[test]
    fn driver_connect() {
        let mut env = SqlEnvironment::new().unwrap();
        print_diagnostics(&env);

        env.set_version(OdbcVersion::SQL_OV_ODBC3_80).unwrap();
        print_diagnostics(&env);

        let arc_env = Arc::new(RwLock::new(env));
        let connection_string = std::env::var("ODBC_CONNECTION_STRING_MSSQL").unwrap();
        let mut connection = SqlConnection::new(&arc_env).unwrap();
        print_diagnostics(&connection);
        print_diagnostics_async(&arc_env);

        connection.driver_connect(&connection_string).unwrap();
        print_diagnostics(&connection);
    }

    #[test]
    fn driver_connect_async() {
        use std::sync::{Arc, RwLock};

        let mut env = SqlEnvironment::new().unwrap();
        print_diagnostics(&env);

        env.set_version(OdbcVersion::SQL_OV_ODBC3_80).unwrap();
        print_diagnostics(&env);

        let arc_env = Arc::new(RwLock::new(env));
        let connection_string = std::env::var("ODBC_CONNECTION_STRING_MSSQL").unwrap();
        let mut connection = SqlConnection::new(&arc_env).unwrap();
        print_diagnostics(&connection);
        print_diagnostics_async(&arc_env);

        connection.set_connect_async(true).unwrap();
        connection.set_async(true).unwrap();
        print_diagnostics(&connection);

        tokio::run(
            connection
                .driver_connect_async(&connection_string)
                .then(|connection_result| {
                    let connection = connection_result.unwrap();
                    print_diagnostics(&connection);
                    Ok(())
                }),
        );
    }

    #[test]
    fn connect_dsn() {
        if let Ok(dsn) = std::env::var("ODBC_DSN") {
            let mut env = SqlEnvironment::new().unwrap();
            print_diagnostics(&env);

            env.set_version(OdbcVersion::SQL_OV_ODBC3_80).unwrap();
            print_diagnostics(&env);

            let arc_env = Arc::new(RwLock::new(env));
            let mut connection = SqlConnection::new(&arc_env).unwrap();
            print_diagnostics(&connection);
            print_diagnostics_async(&arc_env);

            connection.connect_dsn(&dsn).unwrap();
            print_diagnostics(&connection);
        }
    }

    #[test]
    fn read_only() {
        let mut env = SqlEnvironment::new().unwrap();
        print_diagnostics(&env);

        env.set_version(OdbcVersion::SQL_OV_ODBC3_80).unwrap();
        print_diagnostics(&env);

        let arc_env = Arc::new(RwLock::new(env));
        let connection_string = std::env::var("ODBC_CONNECTION_STRING_MSSQL").unwrap();
        let mut connection = SqlConnection::new(&arc_env).unwrap();

        print_diagnostics(&connection);
        print_diagnostics_async(&arc_env);

        assert!(!connection.get_read_only().unwrap());

        connection.set_read_only(true).unwrap();
        connection.driver_connect(&connection_string).unwrap();
        let read_only = connection.get_read_only().unwrap();
        assert!(read_only);

        print_diagnostics(&connection);
    }

    #[test]
    fn autocommit() {
        let mut env = SqlEnvironment::new().unwrap();
        print_diagnostics(&env);

        env.set_version(OdbcVersion::SQL_OV_ODBC3_80).unwrap();
        print_diagnostics(&env);

        let arc_env = Arc::new(RwLock::new(env));
        let connection_string = std::env::var("ODBC_CONNECTION_STRING_MSSQL").unwrap();
        let mut connection = SqlConnection::new(&arc_env).unwrap();

        print_diagnostics(&connection);
        print_diagnostics_async(&arc_env);

        connection.set_autocommit(false).unwrap();
        connection.driver_connect(&connection_string).unwrap();
        let autocommit = connection.get_autocommit().unwrap();
        assert!(!autocommit);

        print_diagnostics(&connection);
    }

    #[test]
    fn connection_timeout() {
        let mut env = SqlEnvironment::new().unwrap();
        print_diagnostics(&env);

        env.set_version(OdbcVersion::SQL_OV_ODBC3_80).unwrap();
        print_diagnostics(&env);

        let arc_env = Arc::new(RwLock::new(env));
        let connection_string = std::env::var("ODBC_CONNECTION_STRING_MSSQL").unwrap();
        let mut connection = SqlConnection::new(&arc_env).unwrap();

        print_diagnostics(&connection);
        print_diagnostics_async(&arc_env);

        connection.driver_connect(&connection_string).unwrap();
        assert_eq!(0, connection.get_connection_timeout().unwrap());
        connection.set_connection_timeout(5).unwrap();
        assert_eq!(5, connection.get_connection_timeout().unwrap());

        print_diagnostics(&connection);
    }

    #[test]
    fn dead() {
        let mut env = SqlEnvironment::new().unwrap();
        print_diagnostics(&env);

        env.set_version(OdbcVersion::SQL_OV_ODBC3_80).unwrap();
        print_diagnostics(&env);

        let arc_env = Arc::new(RwLock::new(env));
        let connection_string = std::env::var("ODBC_CONNECTION_STRING_MSSQL").unwrap();
        let mut connection = SqlConnection::new(&arc_env).unwrap();

        print_diagnostics(&connection);
        print_diagnostics_async(&arc_env);

        connection.driver_connect(&connection_string).unwrap();
        let dead = connection.get_dead().unwrap();
        assert!(!dead);

        print_diagnostics(&connection);
    }

    #[test]
    fn connect_dsn_async() {
        use std::sync::{Arc, RwLock};

        if let Ok(dsn) = std::env::var("ODBC_DSN") {
            let mut env = SqlEnvironment::new().unwrap();
            print_diagnostics(&env);

            env.set_version(OdbcVersion::SQL_OV_ODBC3_80).unwrap();
            print_diagnostics(&env);

            let arc_env = Arc::new(RwLock::new(env));
            let mut connection = SqlConnection::new(&arc_env).unwrap();
            print_diagnostics(&connection);
            print_diagnostics_async(&arc_env);

            connection.set_connect_async(true).unwrap();
            connection.set_async(true).unwrap();
            print_diagnostics(&connection);

            tokio::run(
                connection
                    .connect_dsn_async(&dsn)
                    .then(|connection_result| {
                        let connection = connection_result.unwrap();
                        print_diagnostics(&connection);
                        Ok(())
                    }),
            );
        }
    }
}
